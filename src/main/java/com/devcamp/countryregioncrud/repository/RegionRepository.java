package com.devcamp.countryregioncrud.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.countryregioncrud.model.CRegion;

public interface RegionRepository extends JpaRepository<CRegion, Long>{
    List<CRegion> findByCountryId(Long countryId);
	Optional<CRegion> findByIdAndCountryId(Long id, Long instructorId);
}
